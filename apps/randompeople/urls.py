from django.conf.urls import url

from randompeople.views import CustomerFileView

urlpatterns = [
    url(r'^export_csv/', CustomerFileView.as_view(), name="export_csv"),
]
