# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random
import string

from django.conf import settings
from django.db import migrations, models


# phone generator for more believable data
def makeFirst():
    first_digit = random.randint(1, 9)
    remaining = random.randint(0, 99)
    return first_digit * 100 + remaining


def makeSecond():
    middle = 0
    while middle == 0:
        middle1 = random.randint(0, 8)
        middle2 = random.randint(0, 8)
        middle3 = random.randint(0, 8)
        middle = 100 * middle1 + 10 * middle2 + middle3
    return middle


def makeLast():
    return ''.join(map(str, random.sample(range(10), 4)))


def makePhone():
    return '{0}-{1}-{2}'.format(makeFirst(), makeSecond(), makeLast())


domains = getattr(settings, "EMAIL_DOMAINS", ["gmail.com"])
letters = string.ascii_lowercase[:12]


def get_random_name(letters, length):
    return ''.join(random.choice(letters) for i in range(length))


def generate_random_email(length=10):
    return get_random_name(letters, length) + '@' + random.choice(domains)


def generate_data(apps, schema_editor):
    Customer = apps.get_model("randompeople", "Customer")
    Phone = apps.get_model("randompeople", "Phone")
    Email = apps.get_model("randompeople", "Email")

    # Well, trade off here more inserts with less memory usage
    # or bulk_create()
    # but because of "TZ" we`re loading the ddos minigun for db ;)
    for i in range(0, 21000000):
        customer = Customer.objects.create(
            first_name=random.choice(getattr(settings, "FIRST_NAME_POOL", ["John"])),
            last_name=random.choice(getattr(settings, "SECOND_NAME_POOL", ["Doe"]))
        )
        Phone.objects.create(
            number=makePhone(),
            customer=customer
        )
        Email.objects.create(
            address=generate_random_email(),
            customer=customer
        )


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('randompeople', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(generate_data),
    ]
