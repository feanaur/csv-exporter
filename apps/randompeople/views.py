import csv

from django.views.generic import ListView
from django.http import StreamingHttpResponse
from randompeople.models import Customer
from randompeople.utils import queryset_iterator


class Echo(object):
    def write(self, value):
        return value


class CSVResponseMixin(object):
    def get_queryset(self):
        return queryset_iterator(Customer.objects)

    def render_to_response(self, context, **response_kwargs):
        pseudo_buffer = Echo()
        writer = csv.writer(pseudo_buffer)
        response = StreamingHttpResponse((writer.writerow([customer]) for customer in self.get_queryset()),
                                         content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="customers.csv"'
        return response


class CustomerFileView(CSVResponseMixin, ListView):
    model = Customer
